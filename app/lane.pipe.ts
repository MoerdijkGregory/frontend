import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'lane'
})
export class LanePipe implements PipeTransform {

  transform(value: Array<{lane: string, SumVictory: number, TotVictory: number, pourcentage: number}>, lane: 'Top'|'Mid' | 'Jungle' | 'Adc' | 'Support'): number|any {
    if (!value) return 0;
    let data = value.find(d => d.lane === lane);
    if (!data) return 0;
    return Math.round(data.pourcentage);
  }

}
