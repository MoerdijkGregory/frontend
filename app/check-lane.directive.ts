import { Directive, ElementRef, HostListener, Input } from '@angular/core';
import { FormControl } from '@angular/forms';

@Directive({
  selector: '[appCheckLane]'
})
export class CheckLaneDirective {
  @Input('lane') lane: string;
  @Input('parent') ul: HTMLUListElement;
  @Input('FormControl') formControl: FormControl;


  constructor(private elRef: ElementRef<HTMLLabelElement>) { }


  get isChecked(): boolean {
    return this.label.classList.contains('checked');
  }

  get label(): HTMLLabelElement {
    return this.elRef.nativeElement;
  }

  @HostListener('click')
  setChecked() {
    const labels = this.ul.querySelectorAll('label');
    labels.forEach(label => label.classList.remove('checked'));
    this.label.classList.add('checked');
    this.formControl.setValue(this.lane);
  }

  unChecked() {
    console.log("TEST");
    this.label.classList.remove('checked');
  }
}
