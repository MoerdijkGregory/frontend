import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { Observable } from 'rxjs';
import { FormGroup, FormBuilder, Validators, FormControl, ValidatorFn, ValidationErrors } from '@angular/forms';
import  {User} from 'src/app/models/User'
import { catchError } from 'rxjs/operators';
import { Router } from '@angular/router';

export const PasswordValidator: ValidatorFn = (group: FormGroup): ValidationErrors |null => {
  const password = group.get('password');
  const passwordVerif = group.get('verifPassword');

  return password.value === passwordVerif.value ? null : {'samePwd': false}
}

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {

  constructor(private httpclient : HttpClient, private formBuilder: FormBuilder, private router: Router ) { }
  AddUserUrl = environment.domaineUrl + "/api/user";
  User : Observable<User>
  registerForm: FormGroup;


    submitRegister(){
      if(this.registerForm.valid) {
        console.log(this.registerForm.value);
        this.addUser(this.registerForm.value).subscribe(data => {


          this.router.navigateByUrl("/home");
        }, error => console.log(error));
       
      } else {
        console.log("INVALID", this.registerForm.value);
      }
      // console.log(form.value);
      // alert("Le formulaire a été soumis");
      // form.reset();
    }
    addUser (user: User): Observable<User> {
      return this.httpclient.post<User>(this.AddUserUrl, user);
    }


  

  ngOnInit(): void {
    this.registerForm = this.formBuilder.group({
      pseudo: new FormControl('', [Validators.required]),
      email: new FormControl('', [Validators.required, Validators.email]),
      password: new FormControl('', [Validators.required]),
      verifPassword: new FormControl('', [Validators.required]),
      rank: new FormControl('', [Validators.required]),
      avatar: new FormControl('', [Validators.required]),
    }, {validators: PasswordValidator})

  }

}
