import { Component, OnInit, AfterViewInit } from '@angular/core';
import { Champion } from 'src/app/models/champion';
import { ActivatedRoute } from '@angular/router';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { ModalService } from '../_modal';

@Component({
  selector: 'app-champ-details',
  templateUrl: './champ-details.component.html',
  styleUrls: ['./champ-details.component.css']
})
export class ChampDetailsComponent implements OnInit {

  constructor(private route: ActivatedRoute , private httpClient: HttpClient, private modalService: ModalService) { }

  tipList : Observable<any>;

  champions : Observable<any>;
  model : Champion;
  name: string;
  counter : any;
  currentChamp: string = `../../assets/image/champion/chooseChamp.png`
  private champListUrl = environment.domaineUrl + "/api/champion";
  

  ngOnInit(): void {
    this.name = this.route.snapshot.paramMap.get('name');
    this.httpClient.get<Champion>("http://localhost:8000/api/champion/" + this.name).subscribe(data => this.model = data);
    this.httpClient.get<Champion>("http://localhost:8000/api/counter/" + this.name).subscribe(data => this.counter = data);
    this.champions = this.httpClient.get(this.champListUrl)
  }

  /**
   * Ouvre un popup
   * @param {string} id du modal
   * @throws {Error} si id < 1 
   * @returns {(Observable<Tip>|null)} revnvoie un observable de tips si les tip sont dans la db
   */
  openModal(id: string) {
    this.modalService.open(id);
}

closeModal(id: string) {
    this.modalService.close(id);
}
changePic(champ: string, id: string){
  this.currentChamp = `../../../assets/image/champion/${champ}`;
  this.closeModal(id);
}


}
