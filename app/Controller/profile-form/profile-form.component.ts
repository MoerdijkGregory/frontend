import { Component, OnInit, ViewChildren, QueryList, ViewChild, ElementRef } from '@angular/core';
import { FormBuilder, FormGroup, FormControl, Validators } from '@angular/forms';
import { CheckLaneDirective } from '../../check-lane.directive';
import { ModalService } from '../_modal/modal.service';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs/internal/Observable';
import { environment } from 'src/environments/environment';
import { Champion } from 'src/app/models/champion';
import { MatchUp } from 'src/app/models/MatchUp';
import { Tips } from 'src/app/models/Tips';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-profile-form',
  templateUrl: './profile-form.component.html',
  styleUrls: ['./profile-form.component.css']
})
export class ProfileFormComponent implements OnInit {
  @ViewChildren(CheckLaneDirective) lanepicks: QueryList<CheckLaneDirective>;

  form: FormGroup;
  constructor(private formBuilder: FormBuilder, private modalService: ModalService, private httpClient: HttpClient, private route: ActivatedRoute ) { }

  currentChamp: string = `../../assets/image/chooseChamp.png`
  champions : Observable<any>;
  AddMatchUpUrl = environment.domaineUrl + "/api/matchup";
  private champListUrl = environment.domaineUrl + "/api/champion";
  private userTipsUrl;
  private generalTipsUrl;
  matchUp : Observable<MatchUp>;
  userTips : Tips;
  generalTips : Tips;
  pseudo : string;
  private tmpTips = [];
  TabChamp : {
    champ : string , champadv : string , who : string , champUrl : string
  }

  updateTipCheckbox(tip: Tips) {
    const index = this.tmpTips.findIndex(t => t.name == tip.name);
    console.log(this.tmpTips, tip, index);
    if (index == -1) {
      this.tmpTips.push(tip.name);
    } else {
      this.tmpTips.splice(index, 1);
    }
  }

  ngOnInit(): void {
    this.route.params.subscribe(params => {
      this.pseudo = params.pseudo;
      this.userTipsUrl = environment.domaineUrl + "/api/usertips/"+ this.pseudo;
      this.httpClient.get<Tips>(this.userTipsUrl).subscribe(data => {
        console.log(data);
        this.userTips = data})
      
        this.generalTipsUrl = environment.domaineUrl + "/api/generaltips/";
        this.httpClient.get<Tips>(this.generalTipsUrl).subscribe(data => {
          console.log(data);
          this.generalTips = data})
    })
    this.form = this.formBuilder.group({
      lane: new FormControl('Top', [Validators.required]),
      victory: new FormControl('', [Validators.required]),
      champ_name: new FormControl('', [Validators.required]),
      champ_adv: new FormControl('', [Validators.required]),
      tips: new FormControl('', [Validators.required]),
    });
    this.champions = this.httpClient.get(this.champListUrl)
    this.TabChamp = {
      champ : `chooseChamp` , champadv : `chooseChamp` , who : "", champUrl :`../../assets/image/champion/`
    }
  }





  openModal(id: string , champId : string) {
    this.TabChamp.who = champId;
    console.log(champId);
    this.modalService.open(id);
}

closeModal(id: string) {
    this.modalService.close(id);
}
changePic(champ: string, id: string){
  if(this.TabChamp.who === "0")
  {
    this.TabChamp.champ = `${champ}`;
    this.form.get("champ_name").setValue(this.TabChamp.champ);
    this.form.get("champ_name").updateValueAndValidity();
  }
  else
  {
    this.TabChamp.champadv = `${champ}`;
    this.form.get("champ_adv").setValue(this.TabChamp.champadv);
    this.form.get("champ_adv").updateValueAndValidity();
  }
  console.log(this.TabChamp);
  
  this.closeModal(id);
}

  ngAfterViewInit() {
    console.log(this.lanepicks);
  }


  toggleInputAction(lane: string) {
    this.form.get('lane').setValue(lane);
  }

  onSubmitAction() {
    console.log(this.tmpTips);
    console.log(this.tmpTips.join(','));
    this.form.get('tips').setValue(this.tmpTips.join(','));
      if(this.form.valid) {
        console.log(this.form.value);
        this.addMatchUp(this.form.value).subscribe(data => {}, error => console.log(error));
      } else {
        console.log("INVALID", this.form.value);
      }
  }
  addMatchUp(matchUp : MatchUp)
  {
    return this.httpClient.post<MatchUp>(this.AddMatchUpUrl, matchUp);
  }
}
