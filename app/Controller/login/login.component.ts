import { Component, OnInit } from "@angular/core";
import {
  FormBuilder,
  FormGroup,
  FormControl,
  Validators,
} from "@angular/forms";
import { HttpClient } from "@angular/common/http";
// import { AuthService } from "src/app/service/auth.service";
import { Router } from '@angular/router';

@Component({
  selector: "app-login",
  templateUrl: "./login.component.html",
  styleUrls: ["./login.component.css"],
})
export class LoginComponent implements OnInit {
  constructor(private router: Router/*, private auth: AuthService*/) {}

  loginForm = new FormGroup({
    username: new FormControl("", [Validators.required]),
    password: new FormControl("", [Validators.required]),
  });

  handleSubmit() {
    if(this.loginForm.invalid) return;

    //this.auth.authenticate(this.loginForm.value).subscribe((resultat) => {
    //  this.router.navigateByUrl("/");
    //})
  }
  // login(){
  //   console.log("TESTé")
  //   if(this.loginForm.valid) {
  //     const values = this.loginForm.value;
  //     console.log(this.loginForm.value);
  //     //this.addUser(this.loginForm.value).subscribe(data => {}, error => console.log(error));
  //     this.httpClient.post<any>("http://localhost:8000/api/login_check", {username:  values.email, password: values.password}).subscribe(token => {
  //       sessionStorage.setItem('token', token);
  //       console.log(token);
  //     })
  //   } else {
  //     console.log("INVALID", this.loginForm.value);
  //   }
  // }

  ngOnInit(): void {}
}
