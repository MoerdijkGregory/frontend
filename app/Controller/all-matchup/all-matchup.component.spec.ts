import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AllMatchupComponent } from './all-matchup.component';

describe('AllMatchupComponent', () => {
  let component: AllMatchupComponent;
  let fixture: ComponentFixture<AllMatchupComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AllMatchupComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AllMatchupComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
