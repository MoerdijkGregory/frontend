import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ActivatedRoute } from '@angular/router';
import { MatchUp } from 'src/app/models/MatchUp';
import { environment } from 'src/environments/environment';

@Component({
  templateUrl: './all-matchup.component.html',
  styleUrls: ['./all-matchup.component.css']
})
export class AllMatchupComponent implements OnInit {

  constructor(private httpClient: HttpClient, private route: ActivatedRoute ) { }

  matchup : MatchUp;
  pseudo: string;
  private matchUserUrl;

  ngOnInit(): void {
    this.route.params.subscribe(params => {
      this.pseudo = params.pseudo;
      this.matchUserUrl = environment.domaineUrl + "/api/user_matchup/"+ this.pseudo;
      this.httpClient.get<MatchUp>(this.matchUserUrl).subscribe(data => {
        console.log(data);
        this.matchup = data
      });
    });
  }

}
