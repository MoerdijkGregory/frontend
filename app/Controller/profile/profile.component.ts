import { Component, OnInit } from '@angular/core';
import { ModalService } from '../_modal/modal.service';
import { Observable } from 'rxjs/internal/Observable';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { ActivatedRoute } from '@angular/router';
import { MatchUp } from 'src/app/models/MatchUp';
import { User } from 'src/app/models/User';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.css']
})
export class ProfileComponent implements OnInit {

  constructor(private modalService: ModalService, private httpClient: HttpClient, private route: ActivatedRoute ) { }

  currentChamp: string = `../../assets/image/question-mark.png`
  champions : Observable<any>;
  user : User;
  matchup : MatchUp;
  pseudo: string;
  laneAvg : any[]
  private champListUrl = environment.domaineUrl + "/api/champion";
  private userUrl;
  private matchUserUrl;
  private userLaneAvgUrl; 

  
  ngOnInit(): void {
    this.route.params.subscribe(params => {
      this.pseudo = params.pseudo;
      this.userUrl = environment.domaineUrl + "/api/users/"+ this.pseudo;
      this.matchUserUrl = environment.domaineUrl + "/api/user_matchup_last/"+ this.pseudo;
      this.userLaneAvgUrl = environment.domaineUrl + "/api/user_laneAvg/"+ this.pseudo;
      this.champions = this.httpClient.get(this.champListUrl);
      console.log(this.userUrl);
      this.httpClient.get<User>(this.userUrl).subscribe(data => {
        console.log(data);
        this.user = data[0]
      });
      this.httpClient.get<MatchUp>(this.matchUserUrl).subscribe(data => {
        console.log(data);
        this.matchup = data
      });
      this.httpClient.get<any[]>(this.userLaneAvgUrl).subscribe(data =>{ 
        console.log(data)
        this.laneAvg = data
      });
      console.log(this.laneAvg)
    });
  
  }
  openModal(id: string) {
    this.modalService.open(id);
}

closeModal(id: string) {
    this.modalService.close(id);
}
changePic(champ: string, id: string){
  this.currentChamp = `../../../assets/image/champion/${champ}`;
  this.closeModal(id);
}
}
