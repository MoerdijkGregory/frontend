import { Component, OnInit } from '@angular/core';
import {HttpClient} from '@angular/common/http'
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  constructor(private httpclient : HttpClient) { }

  championList : Observable<any>;
  private champListUrl = environment.domaineUrl + "/api/champion";

  environment: any =environment;


  ngOnInit(): void {
    this.championList = this.httpclient.get(this.champListUrl)
  }

}
