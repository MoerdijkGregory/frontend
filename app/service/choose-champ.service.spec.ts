import { TestBed } from '@angular/core/testing';

import { ChooseChampService } from './choose-champ.service';

describe('ChooseChampService', () => {
  let service: ChooseChampService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(ChooseChampService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
