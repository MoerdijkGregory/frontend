export class User {
    public id: number;
    public pseudo: string;
    public email: string;
    public password: string;
    public matchups: object;
    public rank: string;
    public avatar: File;
    public tips: [];
    public roles: string;
    public groupe: string;
}