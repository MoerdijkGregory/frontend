import { Tips } from './Tips';

export class Champion {
    public id: number;
    public image: string;
    public name: string;
    public tips: Tips[];
}