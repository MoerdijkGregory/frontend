export class MatchUp {
    public id: number;
    public champ_name: string;
    public champ_adv: string;
    public victory: string;
    public lane: string;
    public pseudo: string;
    public tips:  [];
}