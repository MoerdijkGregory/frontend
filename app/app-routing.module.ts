import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './Controller/home/home.component';
import { ChampDetailsComponent } from './Controller/champ-details/champ-details.component';
import { RegisterComponent } from './Controller/register/register.component';
import { LoginComponent } from './Controller/login/login.component';
import { TipsComponent } from './Controller/tips/tips.component';
import { StatsComponent } from './Controller/stats/stats.component';
import { ProfileComponent } from './Controller/profile/profile.component';
import { ProfileFormComponent } from './Controller/profile-form/profile-form.component';
import { AllMatchupComponent } from './Controller/all-matchup/all-matchup.component';



const routes: Routes = [
  { path: '', redirectTo: '/home', pathMatch: 'full'},
  { path: 'home', component: HomeComponent, pathMatch: 'full' },
  { path: 'register', component: RegisterComponent, pathMatch: 'full' },
  { path: 'login', component: LoginComponent, pathMatch: 'full' },
  { path: 'tips', component: TipsComponent, pathMatch: 'full' },
  { path: 'stats', component: StatsComponent, pathMatch: 'full' },
  { path: 'user/:pseudo', component: ProfileComponent, pathMatch: 'full' },
  { path: 'user/:pseudo/profileform', component: ProfileFormComponent, pathMatch: 'full' },
  { path: 'user/:pseudo/allmatchup', component: AllMatchupComponent, pathMatch: 'full' },
  { path: ':name', component: ChampDetailsComponent, pathMatch: 'full' },
  
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
