import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HomeComponent } from './Controller/home/home.component';
import { ChampDetailsComponent } from './Controller/champ-details/champ-details.component';
import { LoginComponent } from './Controller/login/login.component';
import { RegisterComponent } from './Controller/register/register.component';
import { TipsComponent } from './Controller/tips/tips.component';
import { StatsComponent } from './Controller/stats/stats.component';
import { ProfileComponent } from './Controller/profile/profile.component';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ModalModule } from '../app/Controller/_modal';
import { ProfileFormComponent } from './Controller/profile-form/profile-form.component';
import { CheckLaneDirective } from './check-lane.directive';
import { LanePipe } from './lane.pipe';
import { TokenInterceptor } from './interceptors/token.interceptor';
import { AllMatchupComponent } from './Controller/all-matchup/all-matchup.component';


@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    ChampDetailsComponent,
    LoginComponent,
    RegisterComponent,
    TipsComponent,
    StatsComponent,
    ProfileComponent,
    ProfileFormComponent,
    CheckLaneDirective,
    LanePipe,
    AllMatchupComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule,
    ModalModule,
  ],
  providers: [{provide: HTTP_INTERCEPTORS, useClass: TokenInterceptor, multi: true}],
  bootstrap: [AppComponent]
})
export class AppModule { }
