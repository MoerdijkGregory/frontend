import { TestBed } from '@angular/core/testing';

import { ChampionResolverService } from './champion-resolver.service';

describe('ChampionResolverService', () => {
  let service: ChampionResolverService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(ChampionResolverService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
