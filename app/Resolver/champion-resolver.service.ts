import { Injectable } from '@angular/core';
import { Champion } from '../models/champion';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class ChampionResolverService  implements Resolve<Champion>{


  constructor(private httpClient: HttpClient) { }


    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<Champion> | Promise<Champion> | Champion {
      console.log(route.paramMap)
      return this.httpClient.get<Champion>("localhost:8000/api/champion/" + route.paramMap.get("name"))
    };
    
}
